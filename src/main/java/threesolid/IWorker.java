// Author: Luke Gayler
//We extended all the the inner interfaces with IWorker to allow for the objects that implement this to select certain ones to use. --Interface Segregation
// We used an interface to ensure it was open for extension but not for modification. --Open Close Principle
//We created  IWorker to ensure it would have the single responsibility of giving Workers the ability to do any of the methods. --Single Responsibility Principle
//Author: Luke Gayler

package threesolid;

interface IWorker extends IFeedable, IWorkable, IRechargeable {
}

interface IWorkable {
	public String work();
}

interface IFeedable{
	public String eat();
}

interface IRechargeable {
    public String recharge();
}
