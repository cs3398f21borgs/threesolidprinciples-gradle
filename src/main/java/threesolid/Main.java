/**
   * Author: Seth Petersen
   * We put the main function and manager function in a seperate file because of the
   * single responsibility principle and because it is easeir to 
   * read when seperated from the rest of the code
**/
package threesolid;

import threesolid.Robot;
import threesolid.IWorker;
import threesolid.Worker;
import threesolid.SuperWorker;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;
public class Main{

public static Manager tsManager = new Manager();

public static void main(String[] args) 
   {
 
      try 
      {
         System.out.format("Starting ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

            try 
      {
         System.out.format("Stopping ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.exit(0);

   }
}
class Manager {
    IWorker worker;

    public void Manager() {

    }
    public void setWorker(IWorker w) {
        worker=w;
    }

    public void manage() {
        worker.work();
    }
}