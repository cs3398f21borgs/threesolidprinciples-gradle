//Author: Christian Solis
//The team decided to seperate the robot class into a diffent file to respect the single responsibility principle.
           
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

import threesolid.IWorker;
           
class Robot implements IWorkable, IRechargeable{
	public String work() {
		return "working";// ....working
	}
    
    public String recharge() {
        return "recharging";// ....recharging
    }
}