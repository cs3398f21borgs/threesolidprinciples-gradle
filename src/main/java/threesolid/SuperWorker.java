//Author: Alejandro Alburjas

package threesolid;
import threesolid.IWorker;

public class SuperWorker implements IWorkable, IFeedable{
	public String work() {
		return "working much more";//.... working much more
	}

	public String eat() {
		return "eating in lunch break";//.... eating in launch break
	}
}

/*
##The SuperWorker class was given its own class file to avoid a collision of responsibility
##with the Worker class since they both share similar functions. It will also allow for the SuperWorker
##class to be open to modification, as well as the worker class separately.
##It implements two interfaces that distinguishes it from the Robot class by segregating to enforce
interface segregation.
*/