/**
   * Author: Robert Balthrop
   *
   * Interface Segregation Principle is satisfied by Worker implementing 
   * only the methods it needs through multiple smaller interfaces - workable and feedable.
   * Single responsibility violated by inclusion of addpositive.
**/
package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

import threesolid.IWorker;


class Worker implements IWorkable, IFeedable{
	private String name = "";

  	public String getName()
  	{
    	return name;
  	}

  	public void setName(String name)
  	{
      	this.name = name;
  	}

  	public String work()
  	{  

  		if (name == "") 
    	{
       		return "I'm working already!"; //"I'm working already!";
    	}
    	else 
    	{
       		return getName() + " is working very hard!";// name + " is working very hard!";
    	}
	}

	public String eat()
	{
		if (name == "") 
    	{
       		return "I'm eating already";// "I'm eating already!";
    	}
    	else 
    	{
    		return getName() + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";// name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
    	}
	}

      public Boolean addpositive(int a, int b)
    {
      if ((a+b) > 0)
        return(true);
      return(false);
    }
}
